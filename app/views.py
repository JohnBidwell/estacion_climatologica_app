# coding=utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from app import app
from flask import Flask,render_template,request,redirect,url_for
from configuraciones import *
from flask import request
import forms
import psycopg2
conn = psycopg2.connect("dbname=%s user=%s password=%s"%(database,user,passwd))
cur = conn.cursor()

def getUnidad(nombre):
	unidad = ""
	if nombre=="Temperatura":
		unidad = "°C".decode('utf-8')
	elif nombre=="Humedad":
		unidad = "%"
	elif nombre=="Pluviómetro":
		unidad="cm3"
	return unidad

def navbar():
	sql = """
		  	select id, nombre from sensors;
	      """
	print sql
	cur.execute(sql)
	sensores_navbar= cur.fetchall()

	# Alertas
	sql = """
		SELECT alertas_aux.id, alertas_aux.value, alertas_aux.status, sensors.nombre, sensors.id
		FROM alertas_aux, sensors, alerts
		WHERE alertas_aux.alerta_id=alerts.id
			AND alerts.sensor_id=sensors.id
			AND alertas_aux.status=true;
	"""
	cur.execute(sql)
	alertas_navbar = cur.fetchall()
	return sensores_navbar, alertas_navbar



@app.route('/')
@app.route('/index')
def index():

	sql ="""
			select id, nombre from sensors order by id asc;
		"""
	print sql
	cur.execute(sql)
	sensores  = cur.fetchall()

	sql ="""
			select value, sensor_id from measures where created_at = CURRENT_TIMESTAMP and created_at>=CURRENT_TIMESTAMP-interval'24 hour' order by created_at asc;
		"""
	print sql
	cur.execute(sql)
	mediciones  = cur.fetchall()

	# mediciones
	# mediciones =
	#	[ [id_sensor, nombre_sensor, unidad, [mediciones], [fechas] , []]
	graficar = []
	sql = """
		SELECT measures.sensor_id, measures.value, measures.created_at, sensors.nombre
		FROM  measures, sensors
		WHERE created_at <= CURRENT_TIMESTAMP
			AND created_at>=CURRENT_TIMESTAMP-interval'8 hour'
			AND measures.sensor_id=sensors.id
			order by measures.sensor_id asc, measures.created_at asc;
	"""
	print sql
	cur.execute(sql)
	mediciones = cur.fetchall()
	#print mediciones
	#print len(mediciones)


	datos_sensor = []
	id_sensor=mediciones[0][0]
	nombre = mediciones[0][3]
	med = [mediciones[0][1]]
	times = [mediciones[0][2].strftime("%Y-%m-%d %H:%M")]
	for index, medicion in enumerate(mediciones):
		if medicion[0] == id_sensor:
			med.append(medicion[1])
			times.append(medicion[2].strftime("%Y-%m-%d %H:%M"))
			print med
		if index+1==len(mediciones) or medicion[0]!=id_sensor:
			unidad = getUnidad(nombre)
			#Agregar mas nombres
			datos_sensor.append(id_sensor)
			datos_sensor.append(nombre)
			datos_sensor.append(unidad)
			datos_sensor.append(med)
			datos_sensor.append(times)
			graficar.append(datos_sensor)
			datos_sensor = []
			id_sensor=medicion[0]
			nombre = medicion[3]
			print nombre
			med = [medicion[1]]
			times = [medicion[2].strftime("%Y-%m-%d %H:%M")]
	print graficar

	sensores_navbar, alertas_navbar = navbar()

	return render_template("index.html", sensores=sensores, graficar=graficar, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)

@app.route('/sensores')
def sensores():
	sql = """
		select * from sensors;
	"""
	print sql
	cur.execute(sql)
	sensores = cur.fetchall()

	sensores_navbar, alertas_navbar = navbar()

	return render_template("sensores.html", sensores=sensores, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)


def line_chart(sensor, mediciones):
	legend = sensor[1]
	values = []
	labels = []
	for medicion in mediciones:
		values.append(medicion[1])
		#print(medicion[3].strftime("%Y-%m-%d %H:%M:%S.%f"))
		labels.append(medicion[3].strftime("%Y-%m-%d %H:%M"))
	#for valor in labels:
		#print valor
	return legend,values,labels
    #return render_template('line_chart.html', values=temperatures, labels=times, legend=legend)

@app.route('/sensores/<id>', methods=['GET', 'POST'])
@app.route('/sensores/<id>/', methods=['GET', 'POST'])
def sensor_info(id, value=24):
	if request.method=='POST':
		sql = """
			UPDATE alertas_aux set(status)=(false)
			FROM alerts, sensors
			WHERE alertas_aux.alerta_id=alerts.id
				AND alerts.sensor_id=sensors.id
				AND sensors.id=%s;
		"""%id
		cur.execute(sql)
		conn.commit()
		return redirect(url_for('sensor_info', id=id))

	sql = """
		select * from sensors where id=%s;
	"""%id
	print sql
	cur.execute(sql)
	sensor = cur.fetchone()

	unidad = getUnidad(sensor[1])
	if sensor[1]=="Temperatura":
		unidad ="°C".decode('utf-8')
	elif sensor[1]=="Humedad":
		unidad ="%"

	if request.args.get('query') != None:
		value = request.args.get('query')

	# mediciones
	sql = """
		select * from  measures where sensor_id=%s and created_at <= CURRENT_TIMESTAMP and created_at>=CURRENT_TIMESTAMP-interval'%s hour' order by created_at asc;
	"""%(id, value)
	print sql
	cur.execute(sql)
	mediciones = cur.fetchall()

	ultima_medicion = None
	hora_ultima_medicion = None
	if len(mediciones)>0:
		ultima_medicion = mediciones[-1][1]
		hora_ultima_medicion = mediciones[-1][3]


	# Cotas
	cotas = """
		select min(value), max(value), round(avg(value)::numeric, 1) from  (select * from  measures where sensor_id=%s and created_at <= CURRENT_TIMESTAMP and created_at>=CURRENT_TIMESTAMP-interval'%s hour') as t1;
		"""%(id, value)
	cur.execute(cotas)
	cotas = cur.fetchone()

	legend, values, labels = line_chart(sensor, mediciones)


	sql = """
		SELECT alertas_aux.value, alertas_aux.created_at from alertas_aux, alerts, sensors
		WHERE alertas_aux.alerta_id=alerts.id
			AND alerts.sensor_id=sensors.id
			AND sensors.id=%s
			AND alertas_aux.status=true
		order by alertas_aux.created_at DESC
		;
	"""%id
	cur.execute(sql)
	alertas=cur.fetchall()
	print alertas

	sensores_navbar, alertas_navbar = navbar()

	return render_template("sensores_info.html", sensor=sensor, mediciones=mediciones, legend=legend, values=values, labels=labels, unidad=unidad, cotas=cotas, ultima_medicion=ultima_medicion, hora_ultima_medicion=hora_ultima_medicion, alertas=alertas, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)

@app.route('/store', methods=['POST', 'GET'])
def store():
	value = request.args.get('value')
	sensor_id = request.args.get('sensor_id')
	sql = """
		insert into measures(value, sensor_id) values(%i, %i);
	"""%(int(value), int(sensor_id))
	print sql
	cur.execute(sql)
	conn.commit()

	sql = """
		SELECT * from alerts;
	"""
	cur.execute(sql)
	alertas = cur.fetchall()
	for alerta in alertas:
		if int(sensor_id) == alerta[1]:
			if int(value) < alerta[3] or int(value) > alerta[2]:
				print "Hola mundo"
				sql = """
					INSERT into alertas_aux(alerta_id, value) values(%i, %i);
				"""%(alerta[0], int(value) )
				cur.execute(sql)
				conn.commit()

	return render_template("store.html")

@app.route('/configuracion/alertas')
def alertas():

	sql = """
		select alerts.id, sensor_id, max_value, min_value, nombre from alerts, sensors where alerts.sensor_id=sensors.id;
	"""
	cur.execute(sql)
	alertas=cur.fetchall()
	sensores_navbar, alertas_navbar = navbar()

	return render_template('/configuracion/alertas/index.html', alertas=alertas, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)



@app.route('/configuracion/alertas/new', methods=['POST', 'GET'])
def nueva_alerta(id=0):
	alerta_form = forms.NuevaAlertaForm(request.form)
	if request.method == 'POST':
		sql = """ insert into alerts(sensor_id, min_value, max_value)
		values(%i, %i, %i)"""%(int(alerta_form.sensor_id.data), int(alerta_form.valor_minimo.data), int(alerta_form.valor_maximo.data))
		cur.execute(sql)
		conn.commit()

		return redirect(url_for('sensor_info', id=alerta_form.sensor_id.data))

	sql = """
		select * from sensors;
	"""
	print sql
	cur.execute(sql)
	sensores = cur.fetchall()

	sensores_navbar, alertas_navbar = navbar()

	return render_template('/configuracion/alertas/new.html', sensores=sensores, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)

@app.route('/configuracion/alertas/<id>', methods=['POST', 'GET'])
def editar_alerta(id):
	alerta_form = forms.NuevaAlertaForm(request.form)
	sql = """
		select alerts.id, sensor_id, max_value, min_value, nombre from alerts, sensors where alerts.id=%s and alerts.sensor_id=sensors.id;
	"""%id
	cur.execute(sql)
	alerta=cur.fetchone()

	if request.method=='POST':
		sql = """
			UPDATE alerts set(min_value, max_value)=(%s, %s) where alerts.id=%s;
		"""%(alerta_form.valor_minimo.data, alerta_form.valor_maximo.data, id)
		cur.execute(sql)
		conn.commit()
		return redirect(url_for('sensor_info', id=alerta[1]))
	sensores_navbar, alertas_navbar = navbar()
	return render_template("/configuracion/alertas/edit.html", alerta=alerta, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)

@app.route('/configuracion')
def configuracion():
	sensores_navbar, alertas_navbar = navbar()
	return render_template("configuracion/index.html", sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)

@app.route('/configuracion/sensores', methods=['POST', 'GET'])
def configuracion_sensores():
	nuevo_sensor = forms.NuevoSensorForm(request.form)
	if request.method == 'POST':
		sql = """
			insert into sensors(nombre, description) values('%s', '%s') returning id;
		"""%(nuevo_sensor.nombre.data, nuevo_sensor.description.data)
		print sql
		cur.execute(sql)
		conn.commit()
		id = cur.fetchone()[0]

		return redirect(url_for('sensor_info', id=id))

	sql = """
		SELECT * FROM sensors;
	"""
	cur.execute(sql)
	sensores = cur.fetchall()

	sensores_navbar, alertas_navbar = navbar()

	return render_template('configuracion/sensores/index.html', sensores=sensores, sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)


@app.route('/configuracion/agregar_sensor')
def agregarSensor(id=0):
	sensores_navbar, alertas_navbar = navbar()
	return render_template("configuracion/sensores/nuevo_sensor.html", sensores_navbar=sensores_navbar, alertas_navbar=alertas_navbar)


@
