# coding=utf-8
from configuraciones import *
import psycopg2

conn = psycopg2.connect("dbname=%s user=%s password=%s"%(database,user,passwd))
cur = conn.cursor()

sql = """
truncate table measures restart identity;
truncate table sensors restart identity;
truncate table alerts restart identity;
truncate table alertas_aux restart identity;
"""

cur.execute(sql)
conn.commit()

cur.close()
conn.close()
