#coding=utf-8
from wtforms import Form
from wtforms import StringField, IntegerField, validators

class NuevoSensorForm(Form):
    nombre = StringField('Nombre')
    description = StringField('Descripcion')

class NuevaAlertaForm(Form):
    sensor_id = IntegerField('Sensor')
    valor_maximo = IntegerField('Máximo')
    valor_minimo = IntegerField('Mínimo')
