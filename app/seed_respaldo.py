# coding=utf-8
from configuraciones import *
import psycopg2

conn = psycopg2.connect("dbname=%s user=%s password=%s"%(database,user,passwd))
cur = conn.cursor()

sql = """
        insert into sensors(nombre, description) values('Temperatura', 'Medición de Temperatura');
        insert into sensors(nombre, description) values('Humedad', 'Medición de humedad');
        insert into measures(value, sensor_id) values(20, 1);
        insert into measures(value, sensor_id) values(22, 1);
        insert into measures(value, sensor_id) values(21, 1);
        insert into measures(value, sensor_id) values(20, 1);
        insert into measures(value, sensor_id) values(18, 1);
        insert into measures(value, sensor_id) values(16, 1);
        insert into measures(value, sensor_id) values(15, 1);
        insert into measures(value, sensor_id) values(14, 1);
        insert into measures(value, sensor_id) values(14, 1);
        insert into measures(value, sensor_id) values(14, 1);
        insert into measures(value, sensor_id) values(22, 1);
        insert into measures(value, sensor_id) values(8, 1);
        insert into measures(value, sensor_id) values(15, 1);
        insert into measures(value, sensor_id) values(20, 2);
        insert into measures(value, sensor_id) values(22, 2);
        insert into measures(value, sensor_id) values(21, 2);
        insert into measures(value, sensor_id) values(20, 2);
        insert into measures(value, sensor_id) values(18, 2);
        insert into measures(value, sensor_id) values(16, 2);
        insert into measures(value, sensor_id) values(15, 2);
        insert into measures(value, sensor_id) values(14, 2);
        insert into measures(value, sensor_id) values(14, 2);
        insert into measures(value, sensor_id) values(14, 2);
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP + interval '24 hour');
        insert into measures(value, sensor_id, created_at) values(22, 1, CURRENT_TIMESTAMP + interval '24 hour');
        insert into measures(value, sensor_id, created_at) values(21, 1, CURRENT_TIMESTAMP + interval '24 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP + interval '24 hour');
        insert into measures(value, sensor_id, created_at) values(18, 1, CURRENT_TIMESTAMP + interval '24 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '168 hour');
        insert into measures(value, sensor_id, created_at) values(22, 1, CURRENT_TIMESTAMP -interval '168 hour');
        insert into measures(value, sensor_id, created_at) values(21, 1, CURRENT_TIMESTAMP -interval '168 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '168 hour');
        insert into measures(value, sensor_id, created_at) values(18, 1, CURRENT_TIMESTAMP -interval '168 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '336 hour');
        insert into measures(value, sensor_id, created_at) values(22, 1, CURRENT_TIMESTAMP -interval '336 hour');
        insert into measures(value, sensor_id, created_at) values(21, 1, CURRENT_TIMESTAMP -interval '336 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '336 hour');
        insert into measures(value, sensor_id, created_at) values(18, 1, CURRENT_TIMESTAMP -interval '336 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '672 hour');
        insert into measures(value, sensor_id, created_at) values(22, 1, CURRENT_TIMESTAMP -interval '672 hour');
        insert into measures(value, sensor_id, created_at) values(21, 1, CURRENT_TIMESTAMP -interval '672 hour');
        insert into measures(value, sensor_id, created_at) values(20, 1, CURRENT_TIMESTAMP -interval '672 hour');
        insert into measures(value, sensor_id, created_at) values(18, 1, CURRENT_TIMESTAMP -interval '672 hour');
        
"""

cur.execute(sql)
conn.commit()

cur.close()
conn.close()
