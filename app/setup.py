from configuraciones import *
import psycopg2

conn = psycopg2.connect("dbname=%s user=%s password=%s"%(database,user,passwd))
cur = conn.cursor()

sql="""
    DROP SCHEMA public CASCADE;
    CREATE SCHEMA public;
    CREATE TABLE sensors
        (id SERIAL PRIMARY KEY, nombre VARCHAR, description TEXT);
    CREATE TABLE measures
        (id SERIAL PRIMARY KEY, value INTEGER, sensor_id INTEGER, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
    CREATE TABLE alerts
        (id SERIAL PRIMARY KEY, sensor_id INTEGER, max_value INTEGER, min_value INTEGER);
    CREATE TABLE alertas_aux
        (id SERIAL, alerta_id INTEGER, value INTEGER, status bool DEFAULT true, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
    """

cur.execute(sql)
conn.commit()
cur.close()
conn.close()
