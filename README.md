## Requisitos

* Python 2.7
* Flask

#### Clonar repositorio

```bash
git clone git@gitlab.com:JohnBidwellB/estacion_climatologica_app.git "Estación climatológica"
```

#### Acceder a carpeta
```bash
cd "Estación climatológica"
```

#### Ejecutar
```bash
python run.py
```